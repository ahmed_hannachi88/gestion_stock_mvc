package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Category implements Serializable {
	
	@Id
	@GeneratedValue
	private long idCategory ;
	private String code;
	private String designation;
	@OneToMany(mappedBy = "category")
	private List<Article> articles;
	
	public Category() {
		super();
	}

	public long getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(long id) {
		this.idCategory = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public List<Article> getArticle() {
		return articles;
	}

	public void setArticle(List<Article> article) {
		this.articles = article;
	}
	
	

}
